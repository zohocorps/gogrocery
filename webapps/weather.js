document.addEventListener("DOMContentLoaded", async function() {
    document.getElementById("lds-roller").style.display="block";
	await fetch("https://api.open-meteo.com/v1/forecast?latitude=13.0827&longitude=80.2707&current_weather=true")
    .then(response => response.json())
    .then(data => {
        document.getElementById("lds-roller").style.display="none";
        console.log(data)
        var arr = data.current_weather.time.split("T"); 
		var time = arr[1];
        var date = arr[0]
        var temperature = data.current_weather.temperature;
		var location = "Chennai, India";
        document.getElementById("time").innerHTML=time;
        document.getElementById("temperature").innerHTML=temperature;
        // document.getElementById("location").innerHTML=location;
        document.getElementById("full-container").style.display="block";
        document.getElementById("condition").innerHTML="Cloudy";
        document.getElementById("humidity").innerHTML=date;
        document.getElementById("weather-condition").src="images\\sunny.png"
    })
});

async function setcity(city){
    if(city=="Chennai"){
        document.getElementById("lds-roller").style.display="block";
        document.getElementById("full-container").style.display="none";
        await fetch("https://api.open-meteo.com/v1/forecast?latitude=13.0827&longitude=80.2707&current_weather=true")
        .then(response => response.json())
        .then(data => {
            document.getElementById("lds-roller").style.display="none";
            console.log(data)
            var arr = data.current_weather.time.split("T"); 
            var time = arr[1];
            var date = arr[0]
            var temperature = data.current_weather.temperature;
            var location = "Chennai, India";
            document.getElementById("time").innerHTML=time;
            document.getElementById("temperature").innerHTML=temperature;
            document.getElementById("location").innerHTML=location;
            document.getElementById("full-container").style.display="block";
            document.getElementById("condition").innerHTML="Cloudy";
            document.getElementById("humidity").innerHTML=date;
            document.getElementById("weather-condition").src="images\\sunny.png"
        })
    }
    
    else if(city=="Coimbatore"){
        document.getElementById("lds-roller").style.display="block";
        document.getElementById("full-container").style.display="none";
        await fetch("https://api.open-meteo.com/v1/forecast?latitude=11.0168&longitude=76.9558&current_weather=true")
        .then(response => response.json())
        .then(data => {
            document.getElementById("lds-roller").style.display="none";
            console.log(data)
            var arr = data.current_weather.time.split("T"); 
            var time = arr[1];
            var date = arr[0]
            var temperature = data.current_weather.temperature;
            var location = "Coimbatore, India";
            document.getElementById("time").innerHTML=time;
            document.getElementById("temperature").innerHTML=temperature;
            document.getElementById("location").innerHTML=location;
            document.getElementById("full-container").style.display="block";
            document.getElementById("condition").innerHTML="Cloudy";
            document.getElementById("humidity").innerHTML=date;
            document.getElementById("weather-condition").src="images\\sunny.png"
        })
    }

    else if(city=="Madurai"){
        document.getElementById("lds-roller").style.display="block";
        document.getElementById("full-container").style.display="none";

        await fetch("https://api.open-meteo.com/v1/forecast?latitude=9.9252&longitude=78.1198&current_weather=true")
        .then(response => response.json())
        .then(data => {
            document.getElementById("lds-roller").style.display="none";
            console.log(data)
            var arr = data.current_weather.time.split("T"); 
            var time = arr[1];
            var date = arr[0]
            var temperature = data.current_weather.temperature;
            var location = "Madurai, India";
            document.getElementById("time").innerHTML=time;
            document.getElementById("temperature").innerHTML=temperature;
            document.getElementById("location").innerHTML=location;
            document.getElementById("full-container").style.display="block";
            document.getElementById("condition").innerHTML="Cloudy";
            document.getElementById("humidity").innerHTML=date;
            document.getElementById("weather-condition").src="images\\sunny.png"
        })
    }

    else if(city=="Pollachi"){
        document.getElementById("lds-roller").style.display="block";
        document.getElementById("full-container").style.display="none";
        await fetch("https://api.open-meteo.com/v1/forecast?latitude=10.6609&longitude=77.0048&current_weather=true")
        .then(response => response.json())
        .then(data => {
            document.getElementById("lds-roller").style.display="none";
            console.log(data)
            var arr = data.current_weather.time.split("T"); 
            var time = arr[1];
            var date = arr[0];
            var temperature = data.current_weather.temperature;
            var location = "Pollachi, India";
            document.getElementById("time").innerHTML=time;
            document.getElementById("temperature").innerHTML=temperature;
            document.getElementById("location").innerHTML=location;
            document.getElementById("full-container").style.display="block";
            document.getElementById("condition").innerHTML="Cloudy";
            document.getElementById("humidity").innerHTML=date;
            document.getElementById("weather-condition").src="images\\sunny.png"
        })
    }

    else if(city=="Salem"){
        document.getElementById("lds-roller").style.display="block";
        document.getElementById("full-container").style.display="none";
        await fetch("https://api.open-meteo.com/v1/forecast?latitude=11.6643&longitude=78.1460&current_weather=true")
        .then(response => response.json())
        .then(data => {
            document.getElementById("lds-roller").style.display="none";
            console.log(data)
            var arr = data.current_weather.time.split("T"); 
            var time = arr[1];
            var date = arr[0]
            var temperature = data.current_weather.temperature;
            var location = "Salem, India";
            document.getElementById("time").innerHTML=time;
            document.getElementById("temperature").innerHTML=temperature;
            document.getElementById("location").innerHTML=location;
            document.getElementById("full-container").style.display="block";
            document.getElementById("condition").innerHTML="Cloudy";
            document.getElementById("humidity").innerHTML=date;
            document.getElementById("weather-condition").src="images\\sunny.png"
        })
    }
}