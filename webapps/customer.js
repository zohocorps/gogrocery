var temppurchase = [];
async function loginmodal(){
    event.preventDefault();
    var name = document.getElementById("uname").value;
    var pass = document.getElementById("pass").value;
    if(name!="" && pass!=""){
        var name1 = encodeURIComponent(name);
        var pass1 = encodeURIComponent(pass); 
        var action1 = encodeURIComponent("login")
        await fetch(`register?uname=${name1}&psw=${pass1}&action=${action1}`)
        .then((data) => data.json())
        .then((res) => {
            if(res.json){
                if(res.json.error){
                    var uname = document.getElementById("loginerror");
                    uname.innerHTML=res.json.error;
                }
            }
            else if(res.jsonfilter){
                if(res.jsonfilter.error1){
                    uname.innerHTML=res.jsonfilter.error1;
                }
                else if(res.jsonfilter.error2){
                    uname.innerHTML=res.jsonfilter.error2;
                }
            }
            else{
                if(localStorage.getItem("purchaselist")==null){
                    var purchase = JSON.stringify(temppurchase)
                    localStorage.setItem("purchaselist",purchase)
                }
                localStorage.setItem("customername",name)
                var x = document.getElementById("snackbar");
                x.innerHTML="Redirecting to Home Please Wait.....!!!!"
                x.className = "show";
                setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);	
                setTimeout(function(){window.location.href = res.success.redirect;}, 2000);	
            }
        });
        // const login = new XMLHttpRequest();
        // const url = "register";
        // login.open("POST", url, true);
        // login.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        // login.onreadystatechange = function(){
        //     if (login.readyState == XMLHttpRequest.DONE) {
        //         if (login.status == 200) {
        //                 var data = JSON.parse(login.responseText);
        //                 if(data.json){
        //                     if(data.json.error){
        //                         var uname = document.getElementById("loginerror");
        //                         uname.innerHTML=data.json.error;
        //                     }
        //                 }
        //                 else if(data.jsonfilter){
        //                     if(data.jsonfilter.error1){
        //                         uname.innerHTML=data.jsonfilter.error1;
        //                     }
        //                     else if(data.jsonfilter.error2){
        //                         uname.innerHTML=data.jsonfilter.error2;
        //                     }
        //                 }
        //                 else{
		// 					if(localStorage.getItem("purchaselist")==null){
		// 						var purchase = JSON.stringify(temppurchase)
		// 						localStorage.setItem("purchaselist",purchase)
		// 					}
		// 					localStorage.setItem("customername",name)
	    //                     window.location.href = data.success.redirect;
        //                 }
        //         } 
        //         else {
        //             console.log("Error:", login.status);
        //         }
        //     }
        // };
        // login.send("uname=" + name + "&psw=" + pass + "&action=" + action);
    }
    else{
        document.getElementById("loginerror").innerHTML="*fill all fields";
    }
}         


function registermodal(){
	event.preventDefault()
    var name = document.getElementById("unames").value;
    var pass = document.getElementById("psw").value;
    var phno = document.getElementById("phno").value;
    var action = "register"

    if(name!="" && pass!="" && phno!=""){
        if(userpassword(pass) && phvalidation(phno)){
            const requests = new XMLHttpRequest();
            const url = "register";
            requests.open("POST", url, true);
            requests.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            requests.onreadystatechange = function(){
                if (requests.readyState == XMLHttpRequest.DONE) {
                    if (requests.status == 200) {
                            var data = JSON.parse(requests.responseText);
                            if(data.json){
                                if(data.json.error){
                                    console.log("naveen")
                                    var uname = document.getElementById("usererror");
                                    uname.innerHTML=data.json.error;
                                    uname.style.color="red";
                                }
                            }
                            else{
                              if(localStorage.getItem("purchaselist")==null){
								var purchase = JSON.stringify(temppurchase)
								localStorage.setItem("purchaselist",purchase)
							}
							localStorage.setItem("customername",name)
                            var x = document.getElementById("snackbar");
                            x.innerHTML="Redirecting to Home Please Wait.....!!!!"
                            x.className = "show";
                            setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);	
                            setTimeout(function(){window.location.href = data.success.redirect;}, 2000);	
                            }
                    } 
                    else {
                        console.log("Error:", requests.status);
                    }
                }
            };
            requests.send("uname=" + name + 
                "&psw=" + pass +
                "&phno=" + phno+
                "&action=" + action);
            }
	    }
     else{
	    document.getElementById("errormsg1").innerHTML="*fill all fields";
        document.getElementById("errormsg1").style.color="red";

	}
}
function userpassword(){
    var pass = document.getElementById("psw").value;
    let errormsg = document.getElementById("errormsg");
    errormsg.style.color = "red";   
    let alphabetregex = /[A-Z]/;
    let numberregex = /[0-9]/;
    let specialregex = /[\W_]/;
    if(pass.length>=8){
        if(alphabetregex.test(pass)){
            if(numberregex.test(pass)){
                if(specialregex.test(pass)){
                    errormsg.innerHTML="Password ok !!!"
                    errormsg.style.color = "green";
                    return true;
                }
                else{
                    errormsg.innerHTML="password should contain atleast one special character"
                    return false;

                }
            }
            else{
                
                errormsg.innerHTML="*password should contain atleast one numeric value"
                return false;
            }
        }
        else{  
            errormsg.innerHTML="*password should contain atleast one capital letter"
            return false;

        }
    }
    else{
        errormsg.style.color = "red";
        errormsg.innerHTML="*password should contain more than 8 letters"
        return false;
    }
}
function phvalidation(){
    let phno = document.getElementById("phno").value;
    console.log("hello1")
    let errormsg1 = document.getElementById("errormsg1");
    errormsg1.style.color="red";
    let phoneregex = /^[0-9]{10}$/;

    if(phoneregex.test(phno)){
            errormsg1.innerHTML="Mobile Number Valid";
            errormsg1.style.color="green";
            return true
    }
    else{
        document.getElementById("errormsg1").innerHTML="*Mobile Number invalid !!!";
    }
}
function passwordhide(){
    var image = document.getElementById("myimage");
    var pass = document.getElementById("psw");
    if (pass.type=="password"){
        image.src = "images\\eye.png";
        pass.type = "text";
    } else {
        image.src = "images\\eye close.png";
        pass.type = "password";
    }
}
function resets(){
	console.log("hello")
	document.getElementById("unames").value="";
    document.getElementById("psw").value="";
    document.getElementById("phno").value="";
    document.getElementById("errormsg1").innerHTML="";
    document.getElementById("errormsg").innerHTML="";


}
