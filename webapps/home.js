var cartcount=0;
var temppurchases = [];
var products=[];
var purch;
var temp = localStorage.getItem("purchaselist"); 
if(temp!=null && temp!=""){
	 temppurchases = JSON.parse(temp)
}


document.addEventListener("DOMContentLoaded", function() {
	const requests = new XMLHttpRequest();
		const url = "loginvalidate";
		requests.open("GET", url, true);
		requests.onreadystatechange = function(){	
			if (requests.readyState == XMLHttpRequest.DONE) {
				if (requests.status == 200) {
						var data = JSON.parse(requests.responseText)
						if(data.user){
							if(data.user.login){
								console.log(data.user.login)
								document.getElementById("logout").innerHTML="Login";
								document.getElementById("history").style.display="none";
								document.getElementById("abouts").style.display="block";
							}
							else{
								console.log(data.user.logout)
								document.getElementById("logout").innerHTML="Logout";
								document.getElementById("delete").style.display="block";
								document.getElementById("history").style.display="block";
								document.getElementById("abouts").style.display="none";
							}
						}
				} 
				else {
					console.log("Error:", requests.status);
				}
			}
		};
		requests.send(); 
});
function hide(){
  var button = document.getElementById("shopnow");
  if(button.value=="shopnow"){
	  document.getElementById("container").style.transition="0.7s"
      document.getElementById("container").style.display="flex";
      button.value="close";
      button.innerHTML=button.value;
      document.getElementById("image").src ="images\\pngwing.com (1).png"
      document.getElementById("image").style.width="500px"
      console.log(temppurchases)
	  console.log(typeof temppurchases)
  }
  else{
      document.getElementById("container").style.display="none";
      button.value="shopnow";
      button.innerHTML=button.value;
      document.getElementById("image").src ="images\\pngwing.com.png"
	  document.getElementById("image").style.width="500px"
  } 
}

function checkproduct(prodtype){
		document.getElementById("loader").style.display="block";
		document.getElementById("about").style.display="none"
		var prodtype = prodtype
		setTimeout(function() {
			button = document.getElementById("btn");
			const requests = new XMLHttpRequest();
			const url = "home?prodtype="+prodtype;
			requests.open("GET", url, true);
			requests.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			requests.onreadystatechange = function(){
				if (requests.readyState == XMLHttpRequest.DONE) {
					if (requests.status == 200) {
							var data = JSON.parse(requests.responseText);
							document.getElementById("loader").style.display="none";
							if(data.json){
								if(data.json.products){
									products = data.json.products;
									console.log(products);
									var containers = document.getElementById("product-container");
									for (var i = 0; i < products.length; i++){
									  var product = products[i];
									  var div = document.createElement("div");
									  var name = document.createElement("h2");
									  var price = document.createElement("h5");
									  var type = document.createElement("h5");
									  var button = document.createElement("button");
								  
									  div.setAttribute("id","products");
									  div.setAttribute("class","essential-container1");
									  name.setAttribute("id","itemname");
									  name.setAttribute("class","itemname");
									  name.setAttribute("onclick","displayproduct('"+product.itemname+"')");
									  type.setAttribute("id","itemtype");
									  price.setAttribute("id","itemprice");
									  button.setAttribute("value","Add To Cart");
									  button.setAttribute("id",product.itemname+'button');
										button.setAttribute("onclick","addtocart('"+product.itemname+"','"+product.price+"','"+product.type+"')");
										
							  
									  
									  name.textContent=product.itemname;
									  type.textContent="Price : "+product.price;
									  price.textContent="Category : "+product.type;
									  button.textContent="Add To Cart";
			  
									  div.appendChild(name);
									  div.appendChild(type);
									  div.appendChild(price);
									  div.appendChild(button);
									  containers.appendChild(div);
								}
								document.getElementById("backbtn").style.display="block";
								containers.style.display="flex";
								containers.style.alignItems="flex-start";
								containers.style.flexWrap="wrap";
								containers.style.gap="20px";	
  
								
								
								 if(prodtype=="soap"){
									document.getElementById("image").src ="images\\Soap-Bar-PNG.png";
									document.getElementById("image").style.width="500px";
									document.title="Soap";
								 }
								 else if(prodtype=="essential"){
									document.getElementById("image").src ="images\\vegetables.png";
									  document.getElementById("image").style.width="500px";
									document.title="Essential";
								  }
								 else if(prodtype=="chocolates"){
									document.getElementById("image").src ="images\\chocolateside.png";
									document.getElementById("image").style.width="400px";
									document.title="Chocolate";
								 }	
							  }
							}
					} 
					else {
						console.log("Error:", requests.status);
					}
				}
			};
			requests.send();	
		}, 1000);        
}

function reload(){
	document.title="Home";
	document.getElementById("backbtn").style.display="none";
	document.getElementById("image").src ="images\\pngwing.com (1).png";
	document.getElementById("product-container").innerHTML="";
  	document.getElementById("about").style.display="block";
}

function redirectcart(){
	var purchases = JSON.stringify(temppurchases)
	localStorage.setItem("purchaselist",purchases)
	const requests = new XMLHttpRequest();
	const url = "home";
	requests.open("PUT", url, true);
	requests.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	requests.send();
    window.location.href="cart.html";
}




function addtocart(itemname,price,type){	
	var purchase={
			iname:itemname,
			iprice:price,
			icategory:type
	};
	var butt = document.getElementById(itemname+'button');
	if(butt.value=="Add To Cart"){
		
		console.log(temppurchases.indexOf(purchase))
		console.log(temppurchases)
		var c=0;
		for(let i=0;i<temppurchases.length;i++){
			var purchases = temppurchases[i];
			if(purchases.iname==itemname){
				c=1;
				break;
			}
		}
				
		if(c==0){
			butt.innerHTML="Remove";
			butt.value="Remove"
			cartcount+=1;
			temppurchases.push(purchase)
			document.getElementById("purchasediv").style.display="block";
			document.getElementById("purchasecount").innerHTML=cartcount;
			butt.style.backgroundColor="#FDDA0D";
			butt.style.color="black";		
			var x = document.getElementById("snackbar");
			x.innerHTML="Item Added To Cart.....!!!!"
			x.className = "show";
			setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);	
		}
		else{
			var x = document.getElementById("snackbar");
			x.innerHTML="Added Aldready.....!!!!"
			x.className = "show";
			setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);	
			butt.innerHTML="Aldready added";
			butt.style.backgroundColor="rgb(133, 5, 5)";
		}
		
	}
	else{
		butt.innerHTML="Add To Cart";
		butt.value="Add To Cart"
		cartcount-=1;
		document.getElementById("purchasecount").innerHTML=cartcount;
		butt.style.backgroundColor="#023020";
		butt.style.color="white";
		console.log(temppurchases)
		console.log(purchase.iname)
		temppurchases.splice(temppurchases.indexOf(purchase),1);
		
		
		
		console.log(temppurchases)
				
		if(cartcount==0){
			document.getElementById("purchasediv").style.display="none";
		}
		else{
			document.getElementById("purchasediv").style.display="block";
		}
		var x = document.getElementById("snackbar");
		x.innerHTML="Item Removed from Cart.....!!!!"
		x.className = "show";
		setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);	
	}
}
function logout(){
	var log = document.getElementById("logout");
	console.log(log.innerText)
	if(log.innerText=="Login"){
		window.location.href="customer.html";
	}
	else{
		const requests = new XMLHttpRequest();
		const url = "loginvalidate";
		requests.open("PUT", url, true);
		requests.send(); 
		localStorage.removeItem("purchaselist");
		localStorage.removeItem("customername");
		log.innerHTML="Login"
		location.reload();
	}
}

function history(){
	window.location.href="history.html"
}
async function fetchs(){
	await fetch("profile")
	.then(response => response.json())
	.then(data => {
		console.log(data)
		if(data.user){
			if(data.user.customerid){
				document.getElementById("customerid").innerHTML=data.user.customerid;
				document.getElementById("customernames").innerHTML=data.user.customername
				document.getElementById("phone").innerHTML=data.user.phone;
				console.log(data.user.phone)
				document.getElementById("id02").style.display="block";
			}
		}
		else if(data.error){
			if(data.error.usererror){
				alert("username aldready exists..");
			}
		}
	})
}
function profile(){
		var name = document.getElementById("username").value;
		const requests = new XMLHttpRequest();
		const url = "profile?username=" + name;
		requests.open("PUT", url, true);
		requests.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		requests.onreadystatechange = function(){
			if (requests.readyState == XMLHttpRequest.DONE) {
				if (requests.status == 200) {
						var data = JSON.parse(requests.responseText)
						if(data.success){
							if(data.success.user){
								document.getElementById("customernames").innerHTML=name;
								var x = document.getElementById("snackbar");
								x.innerHTML="User Name Changed Sucessfully.....!!!!"
								x.className = "show";
								setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);									
								localStorage.setItem("customername",name);								
							}
						}
						else if(data.error){
							if(data.error.usererror){
								alert("username aldready exists..");
							}
						}
						
				} 
				else {
					console.log("Error:", requests.status);
				}
			}
		};
		requests.send();
}


function delete_account(){
	if(confirm("Are you sure you want to delete your account....All the data will be lost")){
		const requests = new XMLHttpRequest();
		const url = "home";
		requests.open("DELETE", url, true);
		requests.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		requests.onreadystatechange = function(){
			if (requests.readyState == XMLHttpRequest.DONE) {
				if (requests.status == 200) {
						var data = JSON.parse(requests.responseText)
						if(data.json){
							if(data.json.user){
								window.location.href="customer.html";
							}
						}
				} 
				else {
					console.log("Error:", requests.status);
				}
			}
		};
		requests.send();
	}
}

function displayproduct(product){
	purch = product;
	for(let i=0;i<products.length;i++){
		var item = products[i];
		if(item.itemname==product){
			var itemids = item.itemid;
			document.getElementById("id01").style.display="block";
			document.getElementById("productname").innerHTML=product;
			document.getElementById("productprice").innerHTML = item.price;
			document.getElementById("producttype").innerHTML= item.type;
			document.getElementById("description").innerHTML=item.description;
			if(item.type=="soap"){
				document.getElementById("productimage").src="images\\Soap-Bar-PNG.png";
			}
			else if(item.type=="essential"){
				document.getElementById("productimage").src="images\\vegetables.png";
			}
			else{
				document.getElementById("productimage").src="images\\chocolateside.png";
			}
			var butt = document.getElementById(product+'button');
			if(butt.value=="Remove"){
				let prod = document.getElementById("productadd")
				prod.innerHTML="Remove";
				prod.style.color="black";
				prod.style.backgroundColor="#FDDA0D";
				prod.style.border="3px solid #FDDA0D";
			}
			else{
				let prod = document.getElementById("productadd")
				prod.innerHTML="Add To Cart";
				prod.style.color="lightgray";
				prod.style.backgroundColor="#023020";
				prod.style.border="3px solid #023020";
			}
			feedback_function1(itemids);
		}	
	}
}
async function feedback_function1(itemids){
	await fetch("feedback?itemid="+itemids)
    .then(response => response.json())
	.then(parsedData => {
		console.log(parsedData)

		var feedbacksList = parsedData.json.jsonfeedbackArray;
		console.log(feedbacksList);
		document.getElementById("customerfeed").innerHTML="";	
		document.getElementById("loader2").style.display="block";
		setTimeout(function(){
			document.getElementById("loader2").style.display="none";
			if(feedbacksList.length==0){
				document.getElementById("customerfeed").innerHTML="No Feedback Available For this Product.......!";
			}
			else{
				for (var i = 0; i < feedbacksList.length; i++) {
					var feedback = feedbacksList[i];
					var feed = `
					<h4 style="text-align:left;">${feedback.customerref} : </h4>
					<div style="display:flex; margin-left:40px; margin-top:-33px;">
					<h5 style="text-align:left;">${feedback.feedbackcontent}</h5>
					<img src="images/like.png" class="thumbsup" id="likeimage${i}" onclick="likebtn(event,${feedback.feedbackid},${i})">
					<h5 style="margin-left:5px;" id="votes${i}">${feedback.votes}</h5>
					</div>`;	
					document.getElementById("customerfeed").innerHTML+=feed;							
				}
			}				
		},500)
	})
}

async function likebtn(e,feedbackid,i){
	var imgId = e.target.id;
	// console.log(e.target.parent);
	console.log(imgId)
	var images = document.getElementById(imgId);
	console.log(feedbackid)
	if (images.src.includes("unlike.png")){
		let data = {"feedbackid" : feedbackid ,"action":"decrease"}
		const params = new URLSearchParams(data).toString(); 
		await fetch("feedback",{
        method: "POST",
        headers:{
			'Content-Type': 'application/x-www-form-urlencoded'
        },
        body:params
    })
		images.src = images.src.replace("unlike.png", "like.png");
		document.getElementById("votes"+i).innerHTML = parseInt(document.getElementById("votes"+i).innerText)-1 ;
	} 
	else {
		let data = {"feedbackid" : feedbackid ,"action":"increase"}
		const params = new URLSearchParams(data).toString(); 
		await fetch("feedback",{
        method: "POST",
        headers:{
			'Content-Type': 'application/x-www-form-urlencoded'
        },
        body:params
    })
		images.src = images.src.replace("like.png", "unlike.png");
		document.getElementById("votes"+i).innerHTML = parseInt(document.getElementById("votes"+i).innerText)+1 ;
	}
}

function productadd(){	
	var name = document.getElementById("productname").innerText;
	var price  = document.getElementById("productprice").innerText;
	var type = document.getElementById("producttype").innerText;
	var butt = document.getElementById(purch+'button');
	if(butt.value=="Remove"){
		let prod = document.getElementById("productadd")
		prod.innerHTML="Add To Cart";
		prod.style.color="lightgray";
		prod.style.backgroundColor="#023020";
		prod.style.border="3px solid #023020";
	}
	else{
		let prod = document.getElementById("productadd")
		prod.innerHTML="Remove";
		prod.style.color="black";
		prod.style.backgroundColor="#FDDA0D";
		prod.style.border="3px solid #FDDA0D";
	}
	addtocart(name,price,type);
}
function productbuy(){
	var name = document.getElementById("productname").innerText;
	var price  = document.getElementById("productprice").innerText;
	var type = document.getElementById("producttype").innerText;
	let prod = document.getElementById("productadd");
	if(prod.innerHTML!="Remove"){
		addtocart(name,price,type);
	}
	var x = document.getElementById("snackbar");
	x.innerHTML="Redirecting to Cart.....!!!!"
	x.className = "show";
	setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);	
	setTimeout(function(){redirectcart();}, 2000);	
	
}

function addmouseover(){
	var butt = document.getElementById(purch+'button');
	let prod = document.getElementById("productadd")
	if(butt.value=="Remove"){
		prod.style.backgroundColor="lightgray";
		prod.style.border="3px solid #FDDA0D";
		prod.style.color="black";
	}
	else{
		prod.style.color="#023020";
		prod.style.backgroundColor="lightgray";
		prod.style.border="3px solid #023020";
	}
}
function addmouseleave(){
	var butt = document.getElementById(purch+'button');
	if(butt.value=="Remove"){
		let prod = document.getElementById("productadd")
		prod.style.color="black";
		prod.style.backgroundColor="#FDDA0D";
		prod.style.border="3px solid #FDDA0D";
	}
	else{
		let prod = document.getElementById("productadd")
		prod.style.color="lightgray";
		prod.style.backgroundColor="#023020";
		prod.style.border="3px solid #023020";
	}
}
function getItemid(itemname){
	for(let i=0;i<products.length;i++){
		var prod = products[i];
		if(prod.itemname==itemname){
			return prod.itemid;
			break;
		}
	}
} 
function sendfeedback(){
	var feedback = document.getElementById("feedback").value;
	if(feedback!=""){
		var itemname = document.getElementById("productname").innerText;
		var itemid = getItemid(itemname);
		console.log(itemid);
			const requests = new XMLHttpRequest();
			const url = "feedback";
			requests.open("POST", url, true);
			requests.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			requests.onreadystatechange = function(){
				if (requests.readyState == XMLHttpRequest.DONE) {
					if (requests.status == 200) {
						var data = JSON.parse(requests.responseText)
						if(data.json){
							if(data.json.success){
								var x = document.getElementById("snackbar");
								x.innerHTML="Feedback Posted Sucessfully.....!!!!"
								x.className = "show";
								setTimeout(function(){ x.className = x.className.replace("show", ""); }, 2000);		
								console.log(data.json.itemref);
								feedback_function1(data.json.itemref);
							}
							else if(data.json.error){
								alert("Feedback is Empty");
							}
							else{
								alert("Login to Post Feedback....")
							}
						}
					} 
					else {
						console.log("Error:", requests.status);
					}
				}
			};
		requests.send("feedback=" + feedback + "&itemid=" + itemid + "&action=" + "send");
	}
}		