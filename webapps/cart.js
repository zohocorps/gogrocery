var alldivname=[];
var purchaselist=[];

document.addEventListener("DOMContentLoaded", function(){
		const requests = new XMLHttpRequest();
		const url = "loginvalidate";
		requests.open("GET", url, true);
		requests.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		requests.onreadystatechange = function(){	
			if (requests.readyState == XMLHttpRequest.DONE) {
				if (requests.status == 200) {
						var data = JSON.parse(requests.responseText)
						if(data.user){
							if(data.user.login){
								console.log(data.user.login)
								document.getElementById("logout").innerHTML="Login";
							}
							else{
								console.log(data.user.logout)
								document.getElementById("logout").innerHTML="Logout";
							}
						}
				} 
				else {
					console.log("Error:", requests.status);
				}
			}
		};
		requests.send(); 
});
function getItems(){
    var temp = localStorage.getItem("purchaselist"); 
    var customername = localStorage.getItem("customername");
    console.log(temp)
    if(temp!="[]"){
		const requests = new XMLHttpRequest();
			const url = "loginvalidate";
			requests.open("GET", url, true);
			requests.onreadystatechange = function(){	
				if (requests.readyState == XMLHttpRequest.DONE) {
					if (requests.status == 200) {
							var data = JSON.parse(requests.responseText)
							if(data.user){
								if(data.user.login){
									document.getElementById("cartlogin").style.display="block"
								}
								else{
									purchaselist = JSON.parse(temp)
								    console.log(purchaselist)
								    var totprice=0;
								    var containers = document.getElementById("getItems-table");
								    for (let i = 0; i < purchaselist.length; i++){
										        var product = purchaselist[i];
										        var div = document.createElement("tr");
										        var names = document.createElement("td");
										        var price = document.createElement("td");
										        var type = document.createElement("td");
										        var quantity = document.createElement("td");
										        var total = document.createElement("td");
										        var innerdiv = document.createElement("td");
										        var minusimage =  document.createElement("img");
										        var plusimage =  document.createElement("img");
										        var deleteimage = document.createElement("img");
										
										
										
										        div.setAttribute("id","product"+product.iname);
										        div.setAttribute("class","product");
										        names.setAttribute("id",product.iname);
										        price.setAttribute("id","price"+product.iname)
										        type.setAttribute("id","type"+product.iname)
										        quantity.setAttribute("id","quantity"+product.iname);
										        total.setAttribute("id","total"+product.iname);
										
										        innerdiv.setAttribute("id","innerdiv");
										        innerdiv.setAttribute("class","innerdiv");
										
										        plusimage.setAttribute("src","images\\plus.png");
										        minusimage.setAttribute("src","images\\minus.png");
										        plusimage.setAttribute("onclick","quantityset('quantity"+product.iname+"','total"+product.iname+"','plus',"+product.iprice+")");
										        minusimage.setAttribute("onclick","quantityset('quantity"+product.iname+"','total"+product.iname+"','minus',"+product.iprice+")");
										
										        deleteimage.setAttribute("src","images\\delete.png");
										        deleteimage.setAttribute("onclick","deletebtn('"+product.iname+"')");
										
										
										
										
										        names.textContent=product.iname;
										        price.textContent=product.iprice+" rs";
										        type.textContent=product.icategory;
										        quantity.textContent=1;
										        total.textContent=product.iprice;
										
										        innerdiv.appendChild(minusimage)
										        innerdiv.appendChild(quantity)
										        innerdiv.appendChild(plusimage)
										        div.appendChild(names);
										        div.appendChild(type);
										        div.appendChild(price);
										        div.appendChild(innerdiv);
										        div.appendChild(total);
										        div.appendChild(deleteimage)
										        containers.appendChild(div);
										        alldivname.push(product.iname);
										
										        totprice+=parseInt(product.iprice);
							    }
							    document.getElementById("getItems").style.display="block"
							    document.getElementById("getitem-button").style.display="none"
							    document.getElementById("final-price").innerHTML=totprice+" rs";					
								}
							}
					} 
					else {
						console.log("Error:",requests.status);
					}
				}
			};
			requests.send(); 
	}
	else{
		document.getElementById("empty").style.display="block"
	}    
}

function deletebtn(deletediv){
	console.log(deletediv)
    var del = document.getElementById("product"+deletediv)
	$("#product"+deletediv).css('position' , 'absolute');
	$("#product"+deletediv).animate({"left" : '10000px'},2000);
	setTimeout(function(){ 
	del.innerHTML="";
    var sum=0;
    console.log(purchaselist)
    purchaselist.splice(purchaselist.indexOf(deletediv),1);
    console.log(purchaselist)
    if(purchaselist.length!=0){
		alldivname.splice(alldivname.indexOf(deletediv),1);
	    for(let i=0;i<alldivname.length;i++){
	        var finalprice = document.getElementById("total"+alldivname[i]);
	        sum+=parseInt(finalprice.innerText);
	    }
	    document.getElementById("final-price").innerHTML=sum+" rs";
	    
	}
	else{
		document.getElementById("getItems").style.display="none"
    	document.getElementById("getitem-button").style.display="block"
    	document.getElementById("empty").style.display="block"
	}
	var purchase = JSON.stringify(purchaselist)
	localStorage.setItem("purchaselist",purchase)
	var x = document.getElementById("snackbar");
	x.innerHTML="Deleted Successfully.....!!!!"
	x.className = "show";
	setTimeout(function(){ x.className = x.className.replace("show", ""); }, 1000);	
	}, 500);	
    
}

function quantityset(quantity,total,symbol,price){
    var quantity=document.getElementById(quantity)
    var total=document.getElementById(total)
    var count= parseInt(quantity.innerText);
    var totalprice = parseInt(total.innerText)
    if(symbol=="plus"){
        quantity.innerHTML=count+=1;
        total.innerHTML=totalprice+price;
    }
    else{
        if(quantity.innerText>="1"){
            quantity.innerHTML=count-=1;
            total.innerHTML=totalprice-price;
        }
    }
    var sum=0
    for(let i=0;i<alldivname.length;i++){
        console.log("total"+alldivname[i])
        var finalprice = document.getElementById("total"+alldivname[i]);
        sum+=parseInt(finalprice.innerText);
    }
    document.getElementById("final-price").innerHTML=sum+" rs";
}


function confirmPurchase(){
    console.log(alldivname)
    var arr=[];
    var finaltot=0;
    var customername = localStorage.getItem("customername") 
    for(let i=0;i<alldivname.length;i++){
        if(document.getElementById("quantity"+alldivname[i]).innerText!=0){
            var data={
                iname:alldivname[i],
                itype:document.getElementById("type"+alldivname[i]).innerText,
                iprice:document.getElementById("price"+alldivname[i]).innerText,
                iquantity:document.getElementById("quantity"+alldivname[i]).innerText,
                itotal:document.getElementById("total"+alldivname[i]).innerText
            }
            arr.push(data);
            finaltot+=parseInt(document.getElementById("total"+alldivname[i]).innerText);
        }
    }
    
    var user={
		customername:customername,
		finaltotal:finaltot,
		noofitems:alldivname.length
}

    const requests = new XMLHttpRequest();
    const url = "cart";
    requests.open("POST", url, true);
    requests.setRequestHeader("Content-Type", "application/json");
    requests.onreadystatechange = function(){
        if (requests.readyState == XMLHttpRequest.DONE) {
            if (requests.status == 200) {
 				var data = JSON.parse(requests.responseText);
                if(data.success){
                    if(data.success.billno){
						console.log("hello")
						const billno =  data.success.billno;    
						document.getElementById("id01").style.display="block"    
						var purchaselisttemp = []; 
						localStorage.setItem("purchaselist",JSON.stringify(purchaselisttemp));
	 				}
				}
				else{
					alert("Your username is wrong")
					localStorage.removeItem("purchaselist");
					localStorage.removeItem("customername");
					window.location.href="customer.html";
				}
            }
        }
    }
    var json={
		customerdetails:user,
		array:arr
	}

    requests.send(JSON.stringify(json));
}

function reload(){
    var purchase = JSON.stringify(purchaselist)
	localStorage.setItem("purchaselist",purchase)
	window.location.href='home.html';
}

function logincart(){
	window.location.href="customer.html";
}


function logout(){
	var log = document.getElementById("logout");
	console.log(log.innerText)
	if(log.innerText=="Login"){
		window.location.href="customer.html";
	}
	else{
		const requests = new XMLHttpRequest();
		const url = "loginvalidate";
		requests.open("PUT", url, true);
		requests.send(); 		
		localStorage.removeItem("purchaselist");
		localStorage.removeItem("customername");
		log.innerHTML="Login"
		location.reload();
	}
}
								