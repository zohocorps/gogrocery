var cartcount=0;
var billlist=[];
var itemlists=[];
var i;

document.addEventListener("DOMContentLoaded", function() {
	const requests = new XMLHttpRequest();
		const url = "loginvalidate";
		requests.open("GET", url, true);
		requests.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		requests.onreadystatechange = function(){	
			if (requests.readyState == XMLHttpRequest.DONE) {
				if (requests.status == 200) {
						var data = JSON.parse(requests.responseText)
						if(data.user){
							if(data.user.login){
								console.log(data.user.login)
								document.getElementById("logout").innerHTML="Login";
							}
							else{
								console.log(data.user.logout)
								document.getElementById("logout").innerHTML="Logout";
							}
						}
				} 
				else {
					console.log("Error:", requests.status);
				}
			}
		};
		requests.send(); 
});

function reload(){
	window.location.href="home.html";
}

function process(value){
	if(value == "billno"){
		var billno = document.getElementById("billnotext").value;
	}
	else{
		var billno=0;
	}
		const requests = new XMLHttpRequest();
		const url = "history";
		requests.open("POST", url, true);
		requests.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		requests.onreadystatechange = function(){
			if (requests.readyState == XMLHttpRequest.DONE) {
				if (requests.status == 200) {
						var data = JSON.parse(requests.responseText);
						if(data.billnosuccess){
							if(data.billnosuccess.customerid){
								var containers = document.getElementById("table");
								var customerid = data.billnosuccess.customerid;
								var customername = data.billnosuccess.customername;
								var billno = data.billnosuccess.billno;
								var noofitems = data.billnosuccess.noofitems;
								var billtotal = data.billnosuccess.billtotal;
								var itemlist = data.billnosuccess.itemlist;
								console.log(billtotal);
								
								document.getElementById("customerid").innerHTML="Customer id : "+customerid;
								document.getElementById("customername").innerHTML= customername;
								document.getElementById("totals").innerHTML="Bill Total : "+billtotal;
								document.getElementById("billno").innerHTML="Bill No : "+billno;
								document.getElementById("noofitems").innerHTML="Items Purchased : "+noofitems;


								for(let i=0;i<itemlist.length;i++){
									var product = itemlist[i];
									var div = document.createElement("tr");
									var names = document.createElement("td");
									var price = document.createElement("td");
									var type = document.createElement("td");
									var quantity = document.createElement("td");
									
									div.setAttribute("id","product"+product.iname);
									div.setAttribute("class","product");
									names.setAttribute("id",product.iname);
									price.setAttribute("id","price"+product.iname)
									type.setAttribute("id","type"+product.iname)
									quantity.setAttribute("id","quantity"+product.iname);
									
									names.textContent=product.itemname;
					                price.textContent=product.price;
					                type.textContent=product.category;
					                quantity.textContent=product.quantity;
									
									div.appendChild(names);
									div.appendChild(price);
									div.appendChild(quantity);
									div.appendChild(type);
				                    containers.appendChild(div);
								}
								document.getElementById("id02").style.display="block";
								document.getElementById("left").style.display="none"
								document.getElementById("right").style.display="none"
								document.getElementById("errormsg").innerHTML="";
							}
						}
						else if(data.customersuccess){
							if(data.customersuccess.billlist){
								billlist = data.customersuccess.billlist
								itemlists = data.customersuccess.itemlist;
								console.log(billlist)
								console.log(itemlists)
								valid("initial");
							}
						}
						else if(data.countsuccess){
							if(data.countsuccess.count){
								var count = data.countsuccess.count;
								console.log(count)
								document.getElementById("id01").style.display="block";
								document.getElementById("text").innerHTML="The Total Bills Purchased : ";
								document.getElementById("total").innerHTML=count;
								document.getElementById("totalimage").src ="images\\pay.png";
							}
						}
						else if(data.totalsuccess){
							if(data.totalsuccess.total){
								document.getElementById("id01").style.display="block";
								document.getElementById("total").innerHTML=data.totalsuccess.total +" rs";
							}
						}
						else{
							document.getElementById("errormsg").innerHTML=data.billerror.error;
						}
				} 
				else {
					console.log("Error:", requests.status);
				}
			}
		};
		requests.send("billno=" + billno + "&action=" + value);
	}
function inputbill(){
	document.getElementById("container").style.display="none";
	document.getElementById("inputbill").style.display="block";
}
function inputbillback(){
	document.getElementById("container").style.display="block";
	document.getElementById("inputbill").style.display="none";
}

function logout(){
	var log = document.getElementById("logout");
	console.log(log.innerText)
	if(log.innerText=="Login"){
		window.location.href="customer.html";
	}
	else{
		const requests = new XMLHttpRequest();
		const url = "loginvalidate";
		requests.open("PUT", url, true);
		requests.send(); 
		
		localStorage.removeItem("purchaselist");
		localStorage.removeItem("customername");
		log.innerHTML="Login"
		location.reload();
	}
}


function valid(val){
	var c=0;
	var containers = document.getElementById("table");	
	if(val=="initial"){
		i=0;
		c=1;
	}
	else if(val=="next"){
		if(i<billlist.length-1){
			i+=1;
			c=1;
			containers.innerHTML="";
		}
	}
	else{
		if(i>0){
			i-=1;
			c=1
			containers.innerHTML="";
		}
	}
	
	if(c==1){	
	var bill = billlist[i];
	var customerid = bill.customerid;
	var customername = localStorage.getItem("customername");
	var billno = bill.billno;
	var noofitems = bill.noitems;
	var billtotal = bill.total;	
								
	document.getElementById("customerid").innerHTML="Customer id : "+customerid;
	document.getElementById("customername").innerHTML= customername;
	document.getElementById("totals").innerHTML="Bill Total : "+billtotal;
	document.getElementById("billno").innerHTML="Bill No : "+billno;
	document.getElementById("noofitems").innerHTML="Items Purchased : "+noofitems;
								
	var items = itemlists[i]
								
	for(let j=0;j<items.length;j++){
		var product = items[j];
		var div = document.createElement("tr");
		var names = document.createElement("td");
		var price = document.createElement("td");
		var type = document.createElement("td");
		var quantity = document.createElement("td");
										
		div.setAttribute("id","product"+product.itemname);
		div.setAttribute("class","product");
		
		names.textContent=product.itemname;
		price.textContent=product.price;
		type.textContent=product.category;
		quantity.textContent=product.quantity;
										
		div.appendChild(names);
		div.appendChild(price);
		div.appendChild(quantity);
		div.appendChild(type);
		containers.appendChild(div);	
		document.getElementById("id02").style.display="block";		
		document.getElementById("left").style.display="block"
		document.getElementById("right").style.display="block"                    
	   }	
	}	
}
function redirectcart(){
	window.location.href="cart.html";
}


								