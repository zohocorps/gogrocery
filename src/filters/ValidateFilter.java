package filters;
import Logger.Log;
import org.json.JSONObject;
import process.Validate;

import javax.servlet.*;
import java.io.IOException;
import java.io.PrintWriter;

public class ValidateFilter implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        String password = servletRequest.getParameter("psw");
        String action = servletRequest.getParameter("action");
        PrintWriter out = servletResponse.getWriter();
        JSONObject json = new JSONObject();
        String phno = servletRequest.getParameter("phno");
        if(action.equals("register")){
            if(Validate.validate_password(password)){
                if(Validate.phone(phno)){
                    filterChain.doFilter(servletRequest,servletResponse);
                }
                else{
                    json.put("error2","phone number not in proper format");
                    out.println("{\"jsonfilter\":"+ json +"}");
                }
            }
            else {
                json.put("error1","password not in proper format");
                out.println("{\"jsonfilter\":"+ json +"}");
            }
        }
        else{
            filterChain.doFilter(servletRequest,servletResponse);
            Log.Exception_log("response from servlet"+servletResponse);
        }
    }
}
