package Table_Details;

public class ItemListTable {
    public static final String TABLENAME = "billitemlist";

    public static final String ITEMLISTID = "itemlistid";
    public static final String ITEMNAME = "itemname";
    public static final String QUANTITY = "quantity";
    public static final String PRICE = "price";
    public static final String CATEGORY = "category";
    public static final String BILLREF = "billref";
}
