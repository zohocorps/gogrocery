package Table_Details;

public class BillTable {
    public static final String TABLENAME = "bill";
    public static final String BILLNO = "billno";
    public static final String CUSTOMERID = "customerid";
    public static final String TOTAL = "total";
    public static final String NOOFITEMS = "noofitems";
}
