package Table_Details;

public class FeedbackTable {
    public static final String TABLENAME = "feedback";
    public static final String FEEDBACKID = "feedbackid";
    public static final String CONTENT = "feedbackcontent";
    public static final String ITEMREF = "itemref";
    public static final String CUSTOMERREF = "customerref";
    public static final String VOTES = "votes";

}
