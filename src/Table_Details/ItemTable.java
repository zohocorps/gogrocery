package Table_Details;

public class ItemTable {
    public static final String TABLENAME = "items";

    public static final String ITEMID = "itemid";

    public static final String ITEMNAME = "itemname";

    public static final String PRICE = "price";
    public static final String CATEGORY = "category";

    public static final String DESCRIPTION = "descriptions";
   }
