package userclasses;

public class Feedbacks {
    private long feedbackid;
    private long itemref;
    private long votes;
    private String feedbackcontent;
    private String customerref;

    public long getFeedbackid() {
        return feedbackid;
    }

    public void setFeedbackid(long feedbackid) {
        this.feedbackid = feedbackid;
    }

    public long getItemref() {
        return itemref;
    }

    public void setItemref(long itemref) {
        this.itemref = itemref;
    }

    public long getVotes() {
        return votes;
    }

    public void setVotes(long votes) {
        this.votes = votes;
    }

    public String getFeedbackcontent() {
        return feedbackcontent;
    }

    public void setFeedbackcontent(String feedbackcontent) {
        this.feedbackcontent = feedbackcontent;
    }

    public String getCustomerref() {
        return customerref;
    }

    public void setCustomerref(String customerref) {
        this.customerref = customerref;
    }
}