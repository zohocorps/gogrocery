package userclasses;

public class Products {
	long itemid;
    long price;
	String itemname,type,description;
	
	public long getItemid() {
        return itemid;
    }
    public void setItemid(long itemid) {
        this.itemid = itemid;
    }

    public long getPrice() {
        return price;
    }
    public void setPrice(long price) {
        this.price = price;
    }

    public String getItemname() {
        return itemname;
    }

    public void setItemname(String itemname) {
        this.itemname = itemname;
    }

    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getDescription() {
        return description;
    }
}
