package userclasses;

import java.util.regex.Pattern;

public class Customer {
	long customerid;
	String name,phno;
	
	public long getCustomerId() {
        return customerid;
    }

    public void setCustomerId(long customerid) {
        this.customerid = customerid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhno() {
        return phno;
    }

    public void setPhno(String phno) {
        this.phno = phno;
    }




}
