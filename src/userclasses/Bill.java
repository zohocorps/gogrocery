package userclasses;
public class Bill {
    long billno,customerid,noitems;
	long total;
	
	public long getTotal() {
        return total;
    }
    public void setTotal(long total) {
        this.total = total;
    }
	public long getBillno() {
        return billno;
    }
    public void setBillno(long billno) {
        this.billno = billno;
    }

    public long getCustomerid() {
        return customerid;
    }

    public void setCustomerid(long customerid) {
        this.customerid = customerid;
    }
    public long getNoitems() {
        return noitems;
    }
    public void setNoitems(long noitems) {
        this.noitems = noitems;
    }
}
