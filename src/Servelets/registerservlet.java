package Servelets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DB_Operations.Customer_DAO;
import Logger.Log;
import org.json.JSONObject;
import userclasses.Customer;


@WebServlet("/Servelets.registerservlet")
public class registerservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public registerservlet() {
        super();
    }
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String name = request.getParameter("uname");
		String password = request.getParameter("psw");
		PrintWriter out = response.getWriter();
		JSONObject json = new JSONObject();
		JSONObject success = new JSONObject();
		HttpSession session = request.getSession();
		String phno = request.getParameter("phno");
		try {
			if(Customer_DAO.userNameExist(name)){
				long customerid = Customer_DAO.register_CustomerDetails(name,password,phno);
				session.setAttribute("creationTime", System.currentTimeMillis());
				session.setAttribute("customername", name);
				session.setAttribute("password", password);
				session.setAttribute("customerid",customerid);
				session.setAttribute("phone", phno);
				success.put("redirect","home.html");
				success.put("customername",name);
				out.println("{\"success\":"+ success +"}");
				Log.Exception_log("Customer registered Successfully...");
			}
			else {
				json.put("error","*Username aldready exists");
				out.println("{\"json\":"+ json +"}");
				Log.Exception_log("Username already exists..");
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String name = req.getParameter("uname");
		String password = req.getParameter("psw");
		PrintWriter out = resp.getWriter();
		JSONObject json = new JSONObject();
		JSONObject success = new JSONObject();
		HttpSession session = req.getSession();
		Customer customer = null;
		try {
			customer = Customer_DAO.loginCustomer(name, password);
			if(customer==null){
				json.put("error","*Invalid Credentials");
				out.println("{\"json\":"+ json +"}");
				Log.Exception_log("Invalid Credentials....");
			}
			else {
				session.setAttribute("creationTime", System.currentTimeMillis());
				session.setAttribute("customername", name);
				session.setAttribute("password", password);
				session.setAttribute("customerid",customer.getCustomerId());
				session.setAttribute("phone", customer.getPhno());
				success.put("customername",name);
				success.put("redirect","home.html");
				out.println("{\"success\":"+ success +"}");
				Log.Exception_log("Customer log in Success...");
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
