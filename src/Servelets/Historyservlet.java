package Servelets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DB_Operations.Bill_DAO;
import DB_Operations.Itemlist_DAO;
import Logger.Log;
import org.json.JSONObject;
import userclasses.Bill;
import userclasses.Itemlist;

@WebServlet("/Servelets.Historyservlet")
public class Historyservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public Historyservlet() {
        super();
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String billno = request.getParameter("billno");
		String action = request.getParameter("action");
		long total;
		PrintWriter out = response.getWriter();
		HttpSession session = request.getSession();
				
		if(session.getAttribute("customerid")!=null) {
			long customerid = (long) session.getAttribute("customerid");
			String customername = session.getAttribute("customername").toString();
			try {
				if(action.equals("billno")){
					ArrayList<Itemlist> itemlist;
					Bill bill = Bill_DAO.getBill_UsingBillno(Long.parseLong(billno),customerid);
					if(bill!=null) {
						itemlist =  Itemlist_DAO.getItemlist(Integer.parseInt(billno));
						JSONObject jsonsuccess = new JSONObject();
						jsonsuccess.put("billno",bill.getBillno());
						jsonsuccess.put("customername",customername);
						jsonsuccess.put("customerid",bill.getCustomerid());
						jsonsuccess.put("billtotal",bill.getTotal());
						jsonsuccess.put("noofitems",bill.getNoitems());
						jsonsuccess.put("itemlist", itemlist);
						out.println("{\"billnosuccess\":"+ jsonsuccess +"}");
						Log.Exception_log("Bill Returned...");
					}
					else {
						JSONObject billerror = new JSONObject();
						billerror.put("error","Bill number Does'nt exist or the bill cannot be accesed by you");
						out.println("{\"billerror\":"+ billerror +"}");
						Log.Exception_log("Bill number Does'nt exist or the bill cannot be accesed by you...");
					}
				}
				else if(action.equals("customer")){
					ArrayList<Object> returnarray = Bill_DAO.getbill_UsingCustomerid(customerid);
					ArrayList<Bill> billlist = (ArrayList<Bill>) returnarray.get(0);
					ArrayList<ArrayList<Itemlist>> itemlist = (ArrayList<ArrayList<Itemlist>>) returnarray.get(1);
					JSONObject jsonsuccess = new JSONObject();
					jsonsuccess.put("billlist",billlist);
					jsonsuccess.put("itemlist", itemlist);	
					out.println("{\"customersuccess\":"+ jsonsuccess +"}");
					Log.Exception_log("Bill Returned...");
				}
				else if (action.equals("noofbills")){
					int count = 0;
					count = Bill_DAO.getNoOfBills(customerid);
					JSONObject jsoncount = new JSONObject();
					jsoncount.put("count",count);
					out.println("{\"countsuccess\":"+ jsoncount +"}");
					Log.Exception_log("Count Returned...");
				}
				else {
					total = Bill_DAO.getTotalBill(customerid);
					JSONObject jsontotal = new JSONObject();
					jsontotal.put("total", total);
					out.println("{\"totalsuccess\":"+ jsontotal +"}");
					Log.Exception_log("total Returned...");
				}
			} catch (Exception e) {
				Log.Exception_log("Exception Occured",e);
				throw new RuntimeException(e);
			}
		}
		else {
			JSONObject sessionerror = new JSONObject();
			sessionerror.put("sessionerror1","*login to see history");
			out.println("{\"sessionerror\":"+ sessionerror +"}");
			Log.Exception_log("login to see history...");
		}	
	}
}
