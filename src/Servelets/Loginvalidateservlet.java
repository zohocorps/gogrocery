package Servelets;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONObject;
@WebServlet("/Servelets.Loginvalidateservlet")

public class Loginvalidateservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public Loginvalidateservlet() {
        super();
    }
	//hello
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		HttpSession session = req.getSession();
		JSONObject user = new JSONObject();
		PrintWriter out = resp.getWriter();
		if(session.getAttribute("creationTime")!=null){
			long creationTime = (Long) session.getAttribute("creationTime");
			long currentTime = System.currentTimeMillis();
			if ((currentTime - creationTime) / 1000 > 3600) {
				session.invalidate();
				user.put("login", "login");
			}
			else {
				session.setAttribute("creationTime", System.currentTimeMillis());
				user.put("logout", "logout");
			}
		}
		else{
			user.put("login", "login");
		}
		out.println("{\"user\":"+ user +"}");
	}

	protected void doPut(HttpServletRequest request, HttpServletResponse response){
		HttpSession session = request.getSession();
		session.invalidate();
	}
}
