package Servelets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Logger.Log;
import org.json.JSONArray;
import org.json.JSONML;
import org.json.JSONObject;
import process.Add_to_bill;
import process.ProcessRequestJson;
import process.SendNotifications;

@WebServlet("/Servelets.Cartservlet")
public class Cartservlet extends HttpServlet {
	private static final long serialVersionUID = 1L; 

    public Cartservlet() {
        super();
    }
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
   	 	PrintWriter out = response.getWriter();
		BufferedReader reader = request.getReader();
		ProcessRequestJson.ProcessJson(reader);
		
	    long noofitems = ProcessRequestJson.customerDetails.getLong("noofitems");
	    long finaltotal = ProcessRequestJson.customerDetails.getLong("finaltotal");
	    String customername = ProcessRequestJson.customerDetails.getString("customername");

	    

	    HttpSession session = request.getSession();
	    String confirmname = session.getAttribute("customername").toString();
	    long customerid =  (long) session.getAttribute("customerid");
		String phonenumber = session.getAttribute("phone").toString();

	    
	    if(confirmname.equals(customername)) {
	    	 Add_to_bill.bill_values(customerid,finaltotal,noofitems);
	    	 Add_to_bill.itemlist_values();

	 		 JSONObject success = new JSONObject();
	 		 success.put("billno", Add_to_bill.billno);
	 		 out.println("{\"success\":"+ success +"}");
			 Log.Exception_log("Bill added and bill no returned...");
//			try {
//				SendNotifications.triggerEmail("senthilsangeetha2001@gmail.com");
//			} catch (MessagingException e) {
//				throw new RuntimeException(e);
//			}
			SendNotifications.triggerNotification(phonenumber);
		}
	    else {
	 		 JSONObject error = new JSONObject();
	 		 error.put("errormsg","username not exists");
	 		 out.println("{\"error\":"+ error +"}");
			 Log.Exception_log("Username not exists...");
		}
	}
}