package Servelets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DB_Operations.Customer_DAO;
import Logger.Log;
import org.json.JSONException;
import org.json.JSONObject;

@WebServlet("/Servelets.Profileservlet")
public class Profileservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public Profileservlet() {
        super();
    }

	protected void doPut(HttpServletRequest request, HttpServletResponse response) throws IOException {
		PrintWriter out = response.getWriter();
		String username = request.getParameter("username");
		HttpSession session = request.getSession();		
		String customer = (String) session.getAttribute("customername");
		try {
			if(Customer_DAO.userNameExist(username)==false){
				JSONObject error = new JSONObject();
				error.put("usererror","Username exists aldready");
				out.println("{\"error\":"+ error +"}");
				Log.Exception_log("Username exists aldready");
			}
			else {
				Customer_DAO.changeUsername(username,customer);
				session.setAttribute("customername",username);
				JSONObject success = new JSONObject();
				success.put("user","username Changed");
				out.println("{\"success\":"+ success +"}");
				Log.Exception_log("username Changed");
			}
		} catch (Exception e) {
			Log.Exception_log("Exception Occured",e);
			throw new RuntimeException(e);
		}
	}


	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		PrintWriter out = resp.getWriter();
		HttpSession session = req.getSession();
		if(session.getAttribute("customerid")!=null){
			String customer = (String) session.getAttribute("customername");
			JSONObject user = new JSONObject();
			user.put("customerid", session.getAttribute("customerid"));
			user.put("customername", customer);
			user.put("phone", session.getAttribute("phone"));
			out.println("{\"user\":" + user + "}");
			Log.Exception_log("Fetched");
		}
		else{
			JSONObject error = new JSONObject();
			error.put("sessionerror", "Login to view profile");
			out.println("{\"sessionerror\":" + error + "}");
		}
	}
}
