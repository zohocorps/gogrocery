package Servelets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DB_Operations.Customer_DAO;
import DB_Operations.Products_DAO;
import Logger.Log;
import com.adventnet.persistence.DataAccessException;
import org.json.JSONObject;
import userclasses.Products;

@WebServlet("/Servelets.Homeservlet")
public class Homeservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public Homeservlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String prodtype = request.getParameter("prodtype");
		JSONObject json = new JSONObject();
		PrintWriter out = response.getWriter();
		if (prodtype.equals("essential") || prodtype.equals("chocolates") || prodtype.equals("soap")) {
			HttpSession session = request.getSession();
			if(session.getAttribute("products")==null) {
				HashMap<String,ArrayList<Products>> products = new HashMap<>();
				session.setAttribute("products",products);
			}
			HashMap<String,ArrayList<Products>> products = (HashMap<String, ArrayList<Products>>) session.getAttribute("products");
			if(products.get(prodtype)==null){
				ArrayList<Products> productslist;
				try {
					productslist = Products_DAO.getProductList(prodtype);
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
				products.put(prodtype,productslist);
				session.setAttribute("products",products);
			}
			json.put("products", products.get(prodtype));
			out.println("{\"json\":" + json + "}");
			Log.Exception_log("product list returned");
		}
	}

	@Override
	protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession();
		JSONObject json = new JSONObject();
		PrintWriter out = resp.getWriter();
		if (session.getAttribute("customerid") != null) {
			long customerid;
			try {
				customerid = (long) session.getAttribute("customerid");
				Customer_DAO.delete_CustomerDetails(customerid);
				session.invalidate();
				json.put("user", "delete");
				out.println("{\"json\":" + json + "}");
				Log.Exception_log("Deleted");
			} catch (Exception e) {
				Log.Exception_log("Exception Occured",e);
				e.printStackTrace();
			}
		} else {
			json.put("user", "notdelete");
			out.println("{\"json\":" + json + "}");
			Log.Exception_log("Cannot be Deleted");
		}
	}

	@Override
	protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession();
		if(session.getAttribute("products")!=null){
			session.removeAttribute("products");
		}
	}
}
