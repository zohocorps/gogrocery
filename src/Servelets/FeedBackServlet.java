package Servelets;
import DB_Operations.Feedback_DAO;
import Logger.Log;
import com.adventnet.persistence.DataAccessException;
import org.json.JSONArray;
import org.json.JSONObject;
import userclasses.Feedbacks;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class FeedBackServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        if(action.equals("send")){
            String feedback = request.getParameter("feedback");
            int itemId = Integer.parseInt(request.getParameter("itemid"));
            HttpSession session = request.getSession();
            JSONObject json = new JSONObject();
            PrintWriter out = response.getWriter();
            if (session.getAttribute("customerid") != null){
                System.out.println(session.getAttribute("customerid"));
                String customername = session.getAttribute("customername").toString();
                if(feedback!=null || feedback!=""){
                    try {
                        Feedbacks feed = Feedback_DAO.postFeedback(feedback,customername,itemId);
                        json.put("success","success");
                        json.put("feedbackid",feed.getFeedbackid());
                        json.put("feedbackcontent",feed.getFeedbackcontent());
                        json.put("itemref",feed.getItemref());
                        json.put("customerref",feed.getCustomerref());
                        json.put("votes",feed.getVotes());
                        out.println("{\"json\":" + json + "}");
                        Log.Exception_log("feedback successful");
                    } catch (DataAccessException e) {
                        Log.Exception_log("Exception Occured",e);
                        throw new RuntimeException(e);
                    }
                }
                else{
                    json.put("error1","Feedback is Empty");
                    out.println("{\"json\":" + json + "}");
                    Log.Exception_log("Feedback is Empty...");
                }
            }
            else{
                json.put("sessionerror","Login to add Feedback");
                out.println("{\"json\":" + json + "}");
                Log.Exception_log("Login to add Feedback...");
            }
        }
       else{
            String feedbackid = request.getParameter("feedbackid");
            try {
                Feedback_DAO.getVotes(Long.parseLong(feedbackid),action);
            }
            catch (Exception e) {
                Log.Exception_log("Exception Occured",e);
                throw new RuntimeException(e);
            }
       }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws  IOException {
        String itemid = req.getParameter("itemid");
        HttpSession session = req.getSession();
        JSONObject json = new JSONObject();
        PrintWriter out = resp.getWriter();
        if(session.getAttribute("customerid")!=null){
            try {
                ArrayList<Feedbacks> feedbacksList =  Feedback_DAO.getFeedback(Long.parseLong(itemid));
                JSONArray jsonArray = new JSONArray();
                for (Feedbacks feedback : feedbacksList) {
                    JSONObject jsonFeedback = new JSONObject();
                    jsonFeedback.put("feedbackid", feedback.getFeedbackid());
                    jsonFeedback.put("feedbackcontent", feedback.getFeedbackcontent());
                    jsonFeedback.put("customerref", feedback.getCustomerref());
                    jsonFeedback.put("itemref", feedback.getItemref());
                    jsonFeedback.put("votes", feedback.getVotes());
                    jsonArray.put(jsonFeedback);
                }
                json.put("feedbackList", feedbacksList);
                json.put("jsonfeedbackArray",jsonArray);
                json.put("customerid",session.getAttribute("customerid"));
                out.println("{\"json\":" + json + "}");
                Log.Exception_log("Feedback List Returned...");
            } catch (DataAccessException e) {
                Log.Exception_log("Exception Occured",e);
                throw new RuntimeException(e);
            }
        }
        else{
            json.put("error", "Login to getFeedback");
            out.println("{\"json\":" + json + "}");
            Log.Exception_log("Login to add Feedback...");
        }
    }
}
