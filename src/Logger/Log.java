package Logger;

import java.io.File;
import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class Log {
    static Logger log = Logger.getLogger("Log");
    static FileHandler fileHandler = null;
    static {
        try {
            fileHandler = new FileHandler("D:"+File.separator+"AdventNet"+File.separator+"MickeyLite"+File.separator+"logs"+File.separator+"Userlogger.txt");
            log.addHandler(fileHandler);
            SimpleFormatter simpleFormatter = new SimpleFormatter();
            fileHandler.setFormatter(simpleFormatter);
            log.info("Logger initialized....");
        } catch (IOException e) {
            log.log(Level.WARNING,"IOException",e);
        }
    }


    public static void Exception_log(String value){
        log.info(value);
    }
    public static void Exception_log(String value,Exception e){
        log.log(Level.WARNING,value,e);
    }
}
