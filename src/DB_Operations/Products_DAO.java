package DB_Operations;
import Table_Details.ItemTable;
import com.adventnet.ds.query.SelectQuery;
import com.adventnet.ds.query.SelectQueryImpl;
import com.adventnet.ds.query.Table;
import com.adventnet.mfw.bean.BeanUtil;
import com.adventnet.persistence.*;
import userclasses.Products;
import java.util.ArrayList;
import java.util.Iterator;
import static DB_Operations.Customer_DAO.addCrieteria;
import static DB_Operations.Customer_DAO.addColumn;

public class Products_DAO {
    static Persistence PERSISTENCE;

    static {
        try {
            PERSISTENCE = (Persistence) BeanUtil.lookup("Persistence");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    public static ArrayList<Products> getProductList(final String prodtype) throws Exception {
        DataObject dataObject;
        SelectQuery selectQuery = new SelectQueryImpl(Table.getTable(ItemTable.TABLENAME));
        addColumn(ItemTable.TABLENAME,selectQuery);
        selectQuery = addCrieteria(ItemTable.TABLENAME,ItemTable.CATEGORY,prodtype,selectQuery);
        dataObject = PERSISTENCE.get(selectQuery);
        final ArrayList<Products> productList = new ArrayList<>();
        Iterator iterator = dataObject.getRows(ItemTable.TABLENAME);
        while (iterator.hasNext()){
            Row result = (Row) iterator.next();
            Products prod = new Products();
            prod.setItemid(result.getLong(1));
            prod.setItemname(result.getString(2));
            prod.setPrice(result.getLong(3));
            prod.setType(result.getString(4));
            prod.setDescription(result.getString(5));
            productList.add(prod);
        }
        return productList;
    }
}
