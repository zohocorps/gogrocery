package DB_Operations;
import Table_Details.ItemListTable;
import com.adventnet.ds.query.SelectQuery;
import com.adventnet.ds.query.SelectQueryImpl;
import com.adventnet.ds.query.Table;
import com.adventnet.mfw.bean.BeanUtil;
import com.adventnet.persistence.DataObject;
import com.adventnet.persistence.Persistence;
import com.adventnet.persistence.Row;
import com.adventnet.persistence.WritableDataObject;
import userclasses.Itemlist;
import java.util.ArrayList;
import java.util.Iterator;
import static DB_Operations.Customer_DAO.addCrieteria;
import static DB_Operations.Customer_DAO.addColumn;

public class Itemlist_DAO {

    static Persistence PERSISTENCE;

    static {
        try {
            PERSISTENCE = (Persistence) BeanUtil.lookup("Persistence");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    public static void store_ItemlistDetails(final long price, final String type, final long quantity, final String name, final long billno) throws Exception {
        Row row = new Row(ItemListTable.TABLENAME);
        row.set(2,name);
        row.set(3,quantity);
        row.set(4,price);
        row.set(5,type);
        row.set(6,billno);
        DataObject dataObject = new WritableDataObject();
        dataObject.addRow(row);
        PERSISTENCE.add(dataObject);
    }
    public static ArrayList<Itemlist> getItemlist(long billno) throws Exception {
        DataObject dataObject;
        SelectQuery selectQuery = new SelectQueryImpl(Table.getTable(ItemListTable.TABLENAME));
        addColumn(ItemListTable.TABLENAME,selectQuery);
        selectQuery = addCrieteria(ItemListTable.TABLENAME,ItemListTable.BILLREF,billno,selectQuery);
        dataObject = PERSISTENCE.get(selectQuery);
        final ArrayList<Itemlist> itemlist = new ArrayList<>();
        Iterator iterator = dataObject.getRows(ItemListTable.TABLENAME);
        while (iterator.hasNext()) {
            Row row = (Row) iterator.next();
            Itemlist prod = new Itemlist();
            prod.setItemname(row.getString(ItemListTable.ITEMNAME));
            prod.setQuantity(row.getLong(ItemListTable.QUANTITY));
            prod.setPrice(row.getLong(ItemListTable.PRICE));
            prod.setCategory(row.getString(ItemListTable.CATEGORY));
            itemlist.add(prod);
        }
        return itemlist;
    }
}
