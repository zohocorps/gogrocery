package DB_Operations;
import Table_Details.BillTable;
import Table_Details.ItemListTable;
import com.adventnet.ds.query.*;
import com.adventnet.mfw.bean.BeanUtil;
import com.adventnet.persistence.*;
import userclasses.Itemlist;
import userclasses.Bill;
import java.util.ArrayList;
import java.util.Iterator;
import static DB_Operations.Customer_DAO.addCrieteria;
import static DB_Operations.Customer_DAO.addColumn;

public class Bill_DAO {
    static Persistence PERSISTENCE;
    static {
        try {
            PERSISTENCE = (Persistence) BeanUtil.lookup("Persistence");
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    public static long store_BillDetails(final long customerid, final long finaltotal, final long noofitems) throws Exception {
        Row row = new Row(BillTable.TABLENAME);
        row.set(BillTable.CUSTOMERID,customerid);
        row.set(BillTable.TOTAL,finaltotal);
        row.set(BillTable.NOOFITEMS,noofitems);

        DataObject dataObject = new WritableDataObject();
        dataObject.addRow(row);
        PERSISTENCE.add(dataObject);
        long billno = row.getLong(BillTable.BILLNO);
        return billno;
    }
    public static ArrayList<Object> getbill_UsingCustomerid(long customerid) throws Exception {
        DataObject dataObject;
        SelectQuery selectQuery = new SelectQueryImpl(Table.getTable(BillTable.TABLENAME));
        addColumn(BillTable.TABLENAME,selectQuery);
        addColumn(ItemListTable.TABLENAME,selectQuery);
        selectQuery = addCrieteria(BillTable.TABLENAME,BillTable.CUSTOMERID, customerid,selectQuery);
        Criteria joinCriteria = new Criteria(
                Column.getColumn(BillTable.TABLENAME,BillTable.BILLNO),
                Column.getColumn(ItemListTable.TABLENAME,ItemListTable.BILLREF),
                QueryConstants.EQUAL
        );
        Join join = new Join(BillTable.TABLENAME,ItemListTable.TABLENAME,joinCriteria,Join.INNER_JOIN);
        selectQuery.addJoin(join);
        dataObject = PERSISTENCE.get(selectQuery);
        ArrayList<Object> returnarray = new ArrayList<>();
        final ArrayList<Bill> billlist = new ArrayList<>();
        final ArrayList<ArrayList<Itemlist>> allitemlists = new ArrayList<>();
        Iterator iterator = dataObject.getRows(BillTable.TABLENAME);
        while (iterator.hasNext()){
            Row billrow = (Row) iterator.next();
            final  Bill bill = new Bill();
            bill.setBillno(billrow.getLong(1));
            bill.setCustomerid(billrow.getLong(2));
            bill.setTotal(billrow.getLong(3));
            bill.setNoitems(billrow.getLong(4));
            final ArrayList<Itemlist> itemlist = new ArrayList<>();
            Iterator iterator1 = dataObject.getRows(ItemListTable.TABLENAME,billrow);
            while (iterator1.hasNext()){
                Row row = (Row) iterator1.next();
                Itemlist items = new Itemlist();
                items.setItemname(row.getString(ItemListTable.ITEMNAME));
                items.setQuantity(row.getLong(ItemListTable.QUANTITY));
                items.setPrice(row.getLong(ItemListTable.PRICE));
                items.setCategory(row.getString(ItemListTable.CATEGORY));
                itemlist.add(items);
            }
            billlist.add(bill);
            allitemlists.add(itemlist);
        }
        returnarray.add(billlist);
        returnarray.add(allitemlists);
        return returnarray;
    }
    public static Bill getBill_UsingBillno(long billno, long customerid) throws Exception {
        DataObject dataObject;
        SelectQuery selectQuery = new SelectQueryImpl(Table.getTable(BillTable.TABLENAME));
        addColumn(BillTable.TABLENAME,selectQuery);
        Criteria criteria1 = new Criteria(Column.getColumn(BillTable.TABLENAME,BillTable.BILLNO),billno,QueryConstants.EQUAL);
        Criteria criteria2 = new Criteria(Column.getColumn(BillTable.TABLENAME,BillTable.CUSTOMERID),customerid,QueryConstants.EQUAL);
        criteria1 = criteria1.and(criteria2);
        selectQuery.setCriteria(criteria1);
        dataObject = PERSISTENCE.get(selectQuery);
        Bill prod = null;
        if (!dataObject.isEmpty()){
            Row result =  dataObject.getRow(BillTable.TABLENAME);
            prod = new Bill();
            prod.setBillno(result.getLong(1));
            prod.setCustomerid(result.getLong(2));
            prod.setTotal(result.getLong(3));
            prod.setNoitems(result.getLong(4));
        }
        return prod;
    }
    public static long getTotalBill(long customerid) throws Exception {
        long total=0;
        DataObject dataObject;
        SelectQuery selectQuery = new SelectQueryImpl(Table.getTable(BillTable.TABLENAME));
        Column column = new Column(BillTable.TABLENAME,BillTable.TOTAL);
        Column column1 = new Column(BillTable.TABLENAME,BillTable.BILLNO);

        selectQuery.addSelectColumn(column);
        selectQuery.addSelectColumn(column1);

        selectQuery = addCrieteria(BillTable.TABLENAME,BillTable.CUSTOMERID, customerid,selectQuery);
        dataObject = PERSISTENCE.get(selectQuery);
        Iterator iterator = dataObject.getRows(BillTable.TABLENAME);
        while (iterator.hasNext()){
            Row row = (Row) iterator.next();
            total+= row.getLong(BillTable.TOTAL);
        }
        return total;
    }
    public static int getNoOfBills(long customerid) throws DataAccessException {
        DataObject dataObject;
        SelectQuery selectQuery = new SelectQueryImpl(Table.getTable(BillTable.TABLENAME));
        Column column = new Column(BillTable.TABLENAME,"*");
        selectQuery.addSelectColumn(column);
        selectQuery = addCrieteria(BillTable.TABLENAME,BillTable.CUSTOMERID,customerid,selectQuery);
        dataObject = PERSISTENCE.get(selectQuery);
        int count = dataObject.size(BillTable.TABLENAME);
        return count;
    }

}
