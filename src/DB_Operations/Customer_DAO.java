package DB_Operations;
import Table_Details.CustomerTable;
import com.adventnet.ds.query.*;
import com.adventnet.mfw.bean.BeanUtil;
import com.adventnet.persistence.*;
import userclasses.Customer;
public class Customer_DAO {
    static Persistence PERSISTENCE;

    static {
        try {
            PERSISTENCE = (Persistence) BeanUtil.lookup("Persistence");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static SelectQuery addColumn(String tablename, SelectQuery selectQuery){
        Column column = new Column(tablename,"*");
        selectQuery.addSelectColumn(column);
        return selectQuery;
    }
    public static SelectQuery addCrieteria(String tablename, String columnname, Object value,SelectQuery selectQuery){
        Criteria criteria = new Criteria(Column.getColumn(tablename,columnname),value,QueryConstants.EQUAL);
        selectQuery.setCriteria(criteria);
        return selectQuery;
    }
    public static boolean userNameExist(final String name) throws Exception{
        DataObject dataObject;
        boolean temp= true;
        SelectQuery selectQuery = new SelectQueryImpl(Table.getTable(CustomerTable.TABLENAME));
        addColumn(CustomerTable.TABLENAME,selectQuery);
        selectQuery = addCrieteria(CustomerTable.TABLENAME,CustomerTable.CUSTOMERNAME,name,selectQuery);
        dataObject = PERSISTENCE.get(selectQuery);
        if(!dataObject.isEmpty()){
            temp=false;
        }
        return temp;
    }
    public static long register_CustomerDetails(final String name, final String password, final String phno) throws Exception {
        Row row = new Row(CustomerTable.TABLENAME);
        row.set(CustomerTable.CUSTOMERNAME,name);
        row.set(CustomerTable.PASSWORD,password);
        row.set(CustomerTable.PHONE,phno);
        DataObject dataObject = new WritableDataObject();
        dataObject.addRow(row);
        PERSISTENCE.add(dataObject);
        long customerid = row.getLong(CustomerTable.CUSTOMERID);
        return customerid;
    }
    public static Customer loginCustomer(final String name, final String password) throws Exception {
        DataObject dataObject;
        Customer customer = null;
        SelectQuery selectQuery = new SelectQueryImpl(Table.getTable(CustomerTable.TABLENAME));
        addColumn(CustomerTable.TABLENAME,selectQuery);
        Criteria criteria1 = new Criteria(Column.getColumn(CustomerTable.TABLENAME,CustomerTable.CUSTOMERNAME),name, QueryConstants.EQUAL);
        Criteria criteria2 = new Criteria(Column.getColumn(CustomerTable.TABLENAME,CustomerTable.PASSWORD),password,QueryConstants.EQUAL);
        criteria1=criteria1.and(criteria2);
        selectQuery.setCriteria(criteria1);
        dataObject = PERSISTENCE.get(selectQuery);
        if(!dataObject.isEmpty()){
            customer = new Customer();
            Row rs =  dataObject.getRow(CustomerTable.TABLENAME);
            customer.setCustomerId(rs.getLong(1));
            customer.setName(rs.getString(2));
            customer.setPhno(rs.getString(4));
        }
        return customer;
    }
    public static void delete_CustomerDetails(long customerid) throws DataAccessException {
        DeleteQuery deleteQuery = new DeleteQueryImpl(CustomerTable.TABLENAME);
        Criteria criteria = new Criteria(Column.getColumn(CustomerTable.TABLENAME,CustomerTable.CUSTOMERID),customerid,QueryConstants.EQUAL);
        deleteQuery.setCriteria(criteria);
        PERSISTENCE.delete(deleteQuery);
    }
    public static void changeUsername(String username, String customer) throws Exception {
        UpdateQuery query = new UpdateQueryImpl(CustomerTable.TABLENAME);
        Criteria criteria = new Criteria(Column.getColumn(CustomerTable.TABLENAME,CustomerTable.CUSTOMERNAME),customer,QueryConstants.EQUAL);
        query.setCriteria(criteria);
        query.setUpdateColumn(CustomerTable.CUSTOMERNAME,username);
        PERSISTENCE.update(query);
    }
}
