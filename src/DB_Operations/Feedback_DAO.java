package DB_Operations;

import Table_Details.FeedbackTable;
import com.adventnet.ds.query.*;
import com.adventnet.mfw.bean.BeanUtil;
import com.adventnet.persistence.*;
import userclasses.Feedbacks;

import java.util.ArrayList;
import java.util.Iterator;

import static DB_Operations.Customer_DAO.*;

public class Feedback_DAO {
    static Persistence PERSISTENCE;

    static {
        try {
            PERSISTENCE = (Persistence) BeanUtil.lookup("Persistence");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    public static Feedbacks postFeedback(String feedback, String customername, int itemid) throws DataAccessException {
        Row row = new Row(FeedbackTable.TABLENAME);
        row.set(FeedbackTable.CONTENT,feedback);
        row.set(FeedbackTable.CUSTOMERREF,customername);
        row.set(FeedbackTable.ITEMREF,itemid);
        DataObject dataObject = new WritableDataObject();
        dataObject.addRow(row);
        PERSISTENCE.update(dataObject);
        Feedbacks feed = new Feedbacks();
        feed.setFeedbackid(row.getLong(FeedbackTable.FEEDBACKID));
        feed.setFeedbackcontent(row.getString(FeedbackTable.CONTENT));
        feed.setCustomerref(row.getString(FeedbackTable.CUSTOMERREF));
        feed.setItemref(itemid);
        feed.setVotes(row.getLong(FeedbackTable.VOTES));
        return feed;
    }
    public static ArrayList<Feedbacks> getFeedback(long itemid) throws DataAccessException {
        DataObject dataObject;
        SelectQuery selectQuery = new SelectQueryImpl(Table.getTable(FeedbackTable.TABLENAME));
        addColumn(FeedbackTable.TABLENAME,selectQuery);
        selectQuery = addCrieteria(FeedbackTable.TABLENAME,FeedbackTable.ITEMREF, itemid,selectQuery);
        dataObject = PERSISTENCE.get(selectQuery);
        ArrayList<Feedbacks> feedbackList = new ArrayList<>();
        Iterator iterator =  dataObject.getRows(FeedbackTable.TABLENAME);
        while (iterator.hasNext()){
            Row result = (Row) iterator.next();
            Feedbacks feedback = new Feedbacks();
            feedback.setFeedbackid(result.getLong(1));
            feedback.setFeedbackcontent(result.getString(2));
            feedback.setItemref(result.getLong(3));
            feedback.setCustomerref(result.getString(4));
            feedback.setVotes(result.getLong(5));
            feedbackList.add(feedback);
        }
        return feedbackList;
    }
    public static void getVotes(long feedbackid,String action) throws DataAccessException {
        DataObject dataObject;
        SelectQuery selectQuery = new SelectQueryImpl(Table.getTable(FeedbackTable.TABLENAME));
        Column column = new Column(FeedbackTable.TABLENAME,FeedbackTable.VOTES);
        selectQuery.addSelectColumn(column);
        Column column1 = new Column(FeedbackTable.TABLENAME,FeedbackTable.FEEDBACKID);
        selectQuery.addSelectColumn(column1);
        selectQuery = addCrieteria(FeedbackTable.TABLENAME,FeedbackTable.FEEDBACKID,feedbackid,selectQuery);
        dataObject = PERSISTENCE.get(selectQuery);
        Row row = dataObject.getRow(FeedbackTable.TABLENAME);
        long votes = row.getLong(FeedbackTable.VOTES);
        UpdateQuery query = new UpdateQueryImpl(FeedbackTable.TABLENAME);
        Criteria criteria = new Criteria(Column.getColumn(FeedbackTable.TABLENAME,FeedbackTable.FEEDBACKID),feedbackid,QueryConstants.EQUAL);
        query.setCriteria(criteria);
        if(action.equals("increase")){
            query.setUpdateColumn(FeedbackTable.VOTES,votes+1);
        }
        else{
            query.setUpdateColumn(FeedbackTable.VOTES,votes-1);
        }
        PERSISTENCE.update(query);
    }
}
