package process;
import java.sql.SQLException;

import DB_Operations.Bill_DAO;
import DB_Operations.Dboperations;
import DB_Operations.Itemlist_DAO;
import com.adventnet.persistence.DataAccessException;
import org.json.JSONObject;
public class Add_to_bill {
	public static long billno;

	public static void bill_values(long customerid, long finaltotal, long noofitems) {
		 try {
	 			billno = Bill_DAO.store_BillDetails(customerid,finaltotal,noofitems);
	 		} 
	 	    catch (ClassNotFoundException | SQLException e) {
	 			e.printStackTrace();
	 		} catch (DataAccessException e) {
			 throw new RuntimeException(e);
		 } catch (Exception e) {
			 throw new RuntimeException(e);
		 }
	}

	public static void itemlist_values() {
		 for (int i = 0; i < ProcessRequestJson.array.length(); i++) {
	 	        try {
	 	        JSONObject item = ProcessRequestJson.array.getJSONObject(i);
					String val = item.getString("iprice");
					long price = Long.parseLong(val.substring(0,val.indexOf("rs")).trim());
	 	        String type = item.getString("itype");
	 	        long quantity = Long.parseLong(item.getString("iquantity"));
	 	        String name = item.getString("iname");
	 			Itemlist_DAO.store_ItemlistDetails(price,type,quantity,name,billno);
	 			} catch (ClassNotFoundException | SQLException e) {
	 				// TODO Auto-generated catch block
	 				e.printStackTrace();
	 			} catch (DataAccessException e) {
					throw new RuntimeException(e);
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
		 }
	}
	
	
}
