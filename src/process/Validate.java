package process;

import java.util.regex.Pattern;

public class Validate {
    public static Boolean validate_password(String pass) {
        String alphabetregex ="(?=.*[a-z])(?=.*[A-Z])";
        String numberregex = "^(?=.*[0-9])";
        String specialregex ="(?=.*[@#$%^&+=])";
        Boolean condition = false;
        if(pass.length()>=8){
            if(Pattern.compile(alphabetregex).matcher(pass).find()){
                if(Pattern.compile(numberregex).matcher(pass).find()){
                    if(Pattern.compile(specialregex).matcher(pass).find()){
                        condition = true;
                    }
                }
            }
        }
        return condition;
    }

    public static boolean phone(String phno2) {
        String phoneregex = "^[0-9]{10}$";
        if(Pattern.compile(phoneregex).matcher(phno2).find()){
            return true;
        }
        else{
            return false;
        }
    }
}
