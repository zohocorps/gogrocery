package process;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Properties;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class SendNotifications {
    public static void triggerEmail(String mailid) throws MessagingException {
        String host = "smtp.gmail.com";
        String from = "naveennaveen@gmail.com";
        String to = mailid;
        String password = "naveen@2001";

        Properties properies = System.getProperties();
        properies.put("mail.smtp.host", host);
        properies.put("mail.smtp.port", "465");
        properies.put("mail.smtp.auth", "true");
        properies.put("mail.smtp.starttls.enable", "true");
        properies.put("mail.smtp.starttls.required", "true");
        properies.put("mail.smtp.ssl.protocols", "TLSv1.2");
        properies.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");

        Authenticator authenticator = new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(from, password);
            }
        };

        Session session = Session.getDefaultInstance(properies ,authenticator);

        MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress(from));
        message.addRecipient(MimeMessage.RecipientType.TO, new InternetAddress(to));
        message.setSubject("Order Placed....!!!!");
        message.setText("Greetings From Go Grocery....\nYour Order requested has been successfully placed.\nKeep using Go Grocery,and get exciting offers and coupons...");

        Transport.send(message);
        System.out.println("Email Sent Successfully");
//        c-Smwq9H7AHasCoj-CjAUgHjJzYT8CIqqpgHKQzP
    }
    public static void triggerNotification(String phonenumber) {
        try {
            String apiKey = "api_key="+"4d4eb888";
            String message = "&text="+"Greetings from Go Grocery....Your Orders have been Placed";
            String sender = "&from="+"Vonage APIs";
            String numbers = "&to="+phonenumber;
            String secret = "&api_secret="+"1uHmin7UlIGErA7Q";


            HttpURLConnection con = (HttpURLConnection) new URL("https://rest.nexmo.com/sms/json?").openConnection();
            String data  = sender+message+numbers+apiKey+secret;
            con.setDoOutput(true);
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-length",Integer.toString(data.length()));
            con.getOutputStream().write(data.getBytes("UTF-8"));


            BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
            StringBuffer buffer = new StringBuffer();
            String line;
            while ((line=reader.readLine())!=null){
                buffer.append(line).append("\n");
            }
            System.out.println(buffer);
            reader.close();
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
//        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
//        Message message = Message.creator(
//                        new com.twilio.type.PhoneNumber("++15856201892"),
//                        new com.twilio.type.PhoneNumber("+919025021960"),
//                        "Your Order is placed")
//                .create();
//
//        System.out.println(message.getSid());
//        }
    }
}
