package process;

import java.io.BufferedReader;
import java.io.IOException;
import org.json.JSONArray;
import org.json.JSONObject;

public class ProcessRequestJson {
	
	static JSONArray array;
	public static JSONObject customerDetails;

	public static String readJson(BufferedReader reader) throws IOException {
		 String json = "";
		 String line = "";
		 while ((line = reader.readLine()) != null) {
		      json += line;
		  }
		  reader.close();
		return json;
	}
	public static void ProcessJson(BufferedReader reader) throws IOException {
		String json = readJson(reader);
	    JSONObject jsonObj = new JSONObject(json);
		array = jsonObj.getJSONArray("array");    
	    customerDetails = jsonObj.getJSONObject("customerdetails");
	}
}
